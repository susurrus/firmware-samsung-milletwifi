Firmware files for the Samsung Galaxy Tab 4 8.0 Wi-Fi

# system
All files copied from `/system/etc/firmware` from a fully updated SM-T330 (Android 5.1.1).
Files from `/system/etc/firmware/wcd9306` were excluded as they're just symlinks.
